/**
 * Created by Josh on 15/07/2016.
 */
module.exports = {
    run: function (creep) {
        //population limits
        var minimumharvesters = 4;
        var minimumupgraders = 4;
        var minimumbuilders = 4;
        var minimumrepairers = 2;
        var minimumwallrep = 1;

        //Ratios
        var numberofharvesters = _.sum(Game.creeps, (c) => c.memory.role == 'harvester');
        var numberofupgraders = _.sum(Game.creeps, (c) => c.memory.role == 'upgrader');
        var numberofbuilders = _.sum(Game.creeps, (c) => c.memory.role == 'builder');
        var numberofrepairers = _.sum(Game.creeps, (c) => c.memory.role == 'repairer');
        var numberofwallrep = _.sum(Game.creeps, (c) => c.memory.role == 'wallrepair');
        
        //minersss
        if (numberofharvesters < minimumharvesters) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE], undefined,
                {role: 'harvester', working: false, caste: 'gcl2'});
        }
        //upgraders
        else if (numberofupgraders < minimumupgraders) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE], undefined,
                {role: 'upgrader', working: false, caste: 'gcl2'});
        }
        //builders
        else if (numberofbuilders < minimumbuilders) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE], undefined,
                {role: 'builder', working: false, caste: 'gcl2'});
        }
        else if (numberofrepairers < minimumrepairers) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE], undefined,
                {role: 'repairer', working: false, caste: 'gcl2'});
        }
        else if (numberofwallrep < minimumwallrep) {
            Game.spawns.Spawn1.createCreep([WORK, WORK, WORK, CARRY, CARRY, MOVE, MOVE], undefined,
                {role: 'wallrepair', working: false, caste: 'gcl2', buildtarget: 'none'});
        }

    }
};

